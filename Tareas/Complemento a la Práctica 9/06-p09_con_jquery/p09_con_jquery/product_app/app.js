// JSON BASE A MOSTRAR EN FORMULARIO
// var baseJSON = {
//     "precio": 0.0,
//     "unidades": 1,
//     "modelo": "XX-000",
//     "marca": "NA",
//     "detalles": "NA",
//     "imagen": "img/default.png"
//   };

$(document).ready(function () {
    let edit = false;
    var arr = Array(5);

    //let JsonString = JSON.stringify(baseJSON,null,2);
    // $('#precio').val(JSON.stringify(baseJSON["precio"], null, 2));
    // $('#unidades').val(JSON.stringify(baseJSON["unidades"], null, 2));
    // $('#modelo').val(JSON.stringify(baseJSON["modelo"], null, 2));
    // $('#marca').val(JSON.stringify(baseJSON["marca"], null, 2));
    // $('#detalles').val(JSON.stringify(baseJSON["detalles"], null, 2));
    // $('#imagen').val(JSON.stringify(baseJSON["imagen"], null, 2));
    $('#product-result').hide();
    listarProductos();

    ///////////////// VALIDACION ////////////////////
    function carac(input, i) {
        var text = $(input).val();
        //Comprobamos la longitud de caracteres
        if (text.length > i) {
            $(input).attr('maxlength', i);
            //$(input).val($(input).val().slice(0, xi))
            $('#arr1').attr('style', 'border: black 2px solid; background-color: red;');
            return false;
        }
        else {
            if ($(input).val()) {
                let search = text;
                $.ajax({
                url: './backend/product-search.php?search=' + text,
                data: {search},
                type: 'GET',
                success: function (response) {
                    if(!response.error) {
                        const prod = JSON.parse(response);

                        if (Object.keys(prod).length > 0) {
                            let alert = '<p>El producto existe <p>';
                            $('#valid').html(alert);
                            $('#valid').show();
                        }
                        else
                        {
                            let alert = '<p>El producto no existe <p>';
                            $('#valid').html(alert);
                            $('#valid').hide();
                        }
                    }
                }
            })
            }
            $('#arr1').attr('style', 'border: black 2px solid; background-color: green;');
            return true;
        }
    }

    $('#name').keyup(function (e) {
        //Comprobamos la longitud de caracteres
        if (!carac("#name", 100)) {
            let search = $('#name').val();
            $.ajax({
                url: './backend/product-search.php?search=' + $('#name').val(),
                data: {search},
                type: 'GET',
                success: function (response) {
                    if(!response.error) {
                        const prod = JSON.parse(response);

                        if (Object.keys(prod).length > 0) {
                            let alert = '<span style="background-color: orange">El producto existe<span>';
                            $('#conteiner').html(alert);
                            $('#conteiner').show();
                        }
                    }
                }
            })
        };

    });

    $('#name').keydown(function (e) {
        //Comprobamos la longitud de caracteres
        if (!carac("#name", 100)) {
        };
    });

    $('#name').change(function (e) {
        if (!carac("#name", 100)) {
            $('#name').val(null);
            arr[0] = false;
            $('#arr1').attr('style', 'border: black 2px solid; background-color: red;');
            alert('Maximo 100 caracteres. Campo no valido');
        }
        else {
            arr[0] = true;
            $('#arr1').attr('style', 'border: black 2px solid; background-color: green;');
            return true;
        }
    });

    $('#precio').change(function (e) {
        precio = $('#precio').val();
        //Comprobamos la longitud de caracteres
        if (precio < 99.99) {
            $('#precio').val(null)
            $('#arr2').attr('style', 'border: black 2px solid; background-color: red;');
            arr[1] = false;
            alert('Precio minimo 99.99');
        }
        else {
            $('#arr2').attr('style', 'border: black 2px solid; background-color: green;');
            arr[1] = true;
        }
    });

    $('#unidades').change(function (e) {
        unid = $('#unidades').val();
        //Comprobamos la longitud de caracteres
        if (unid < 0) {
            $('#unidades').val(null)
            $('#arr3').attr('style', 'border: black 2px solid; background-color: red;');
            arr[2] = false;
            alert('Unidades minimas: 0');
        }
        else {
            $('#arr3').attr('style', 'border: black 2px solid; background-color: green;');
            arr[2] = true;
        }
    });

    $('#modelo').change(function (e) {
        if ($('#modelo').val() == null) {
            $('#arr4').attr('style', 'border: black 2px solid; background-color: red;');
            arr[3] = false;
            alert('Marca no asignada');
        }
       else {
            $('#arr4').attr('style', 'border: black 2px solid; background-color: green;');
            arr[3] = true;
        }
    });

    $('#marca').change(function (e) {
        if ($('#marca').val() == null) {
            $('#arr5').attr('style', 'border: black 2px solid; background-color: red;');
            arr[4] = false;
            alert('Marca no asignada');
        }
       else {
            $('#arr5').attr('style', 'border: black 2px solid; background-color: green;');
            arr[4] = true;
        }
    });

    $('#detalles').change(function (e) {
        if (!carac("#detalles", 250)) {
            $('#detalles').val("NA")
            alert('Maximo 250 caracteres. Campo no valido');
        };
    });

    $('#imagen').change(function (e) {
        if ($('#imagen').val() == null) {
            $('#imagen').val("img/default.png");
            alert('Imagen no asignada');
        }
       else {
            $('#imagen').val("img/" + $('#imagen').val());
        }
    });
    ///////////////// VALIDACION ////////////////////


    function listarProductos() {
        $.ajax({
            url: './backend/product-list.php',
            type: 'GET',
            success: function(response) {
                // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
                const productos = JSON.parse(response);
            
                // SE VERIFICA SI EL OBJETO JSON TIENE DATOS
                if(Object.keys(productos).length > 0) {
                    // SE CREA UNA PLANTILLA PARA CREAR LAS FILAS A INSERTAR EN EL DOCUMENTO HTML
                    let template = '';

                    productos.forEach(producto => {
                        // SE CREA UNA LISTA HTML CON LA DESCRIPCIÓN DEL PRODUCTO
                        let descripcion = '';
                        descripcion += '<li>precio: '+producto.precio+'</li>';
                        descripcion += '<li>unidades: '+producto.unidades+'</li>';
                        descripcion += '<li>modelo: '+producto.modelo+'</li>';
                        descripcion += '<li>marca: '+producto.marca+'</li>';
                        descripcion += '<li>detalles: '+producto.detalles+'</li>';
                    
                        template += `
                            <tr productId="${producto.id}">
                                <td>${producto.id}</td>
                                <td><a href="#" class="product-item">${producto.nombre}</a></td>
                                <td><ul>${descripcion}</ul></td>
                                <td>
                                    <button class="product-delete btn btn-danger" onclick="eliminarProducto()">
                                        Eliminar
                                    </button>
                                </td>
                            </tr>
                        `;
                    });
                    // SE INSERTA LA PLANTILLA EN EL ELEMENTO CON ID "productos"
                    $('#products').html(template);
                }
            }
        });
    };

    $('#search').keyup(function() {
        if($('#search').val()) {
            let search = $('#search').val();
            $.ajax({
                url: './backend/product-search.php?search='+$('#search').val(),
                data: {search},
                type: 'GET',
                success: function (response) {
                    if(!response.error) {
                        // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
                        const productos = JSON.parse(response);
                        
                        // SE VERIFICA SI EL OBJETO JSON TIENE DATOS
                        if(Object.keys(productos).length > 0) {
                            // SE CREA UNA PLANTILLA PARA CREAR LAS FILAS A INSERTAR EN EL DOCUMENTO HTML
                            let template = '';
                            let template_bar = '';

                            productos.forEach(producto => {
                                // SE CREA UNA LISTA HTML CON LA DESCRIPCIÓN DEL PRODUCTO
                                let descripcion = '';
                                descripcion += '<li>precio: '+producto.precio+'</li>';
                                descripcion += '<li>unidades: '+producto.unidades+'</li>';
                                descripcion += '<li>modelo: '+producto.modelo+'</li>';
                                descripcion += '<li>marca: '+producto.marca+'</li>';
                                descripcion += '<li>detalles: '+producto.detalles+'</li>';
                            
                                template += `
                                    <tr productId="${producto.id}">
                                        <td>${producto.id}</td>
                                        <td><a href="#" class="product-item">${producto.nombre}</a></td>
                                        <td><ul>${descripcion}</ul></td>
                                        <td>
                                            <button class="product-delete btn btn-danger">
                                                Eliminar
                                            </button>
                                        </td>
                                    </tr>
                                `;

                                template_bar += `
                                    <li>${producto.nombre}</il>
                                `;
                            });
                            // SE HACE VISIBLE LA BARRA DE ESTADO
                            $('#product-result').show();
                            // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
                            $('#container').html(template_bar);
                            // SE INSERTA LA PLANTILLA EN EL ELEMENTO CON ID "productos"
                            $('#products').html(template);    
                        }
                    }
                }
            });
        }
        else {
            $('#product-result').hide();
        }
    });

    $('#product-form').submit(e => {
        e.preventDefault();
        if (arr[0] == true && arr[1] == true && arr[2] == true && arr[3] == true && arr[4] == true) {
            // SE CONVIERTE EL JSON DE STRING A OBJETO
            //let postData = JSON.parse( $('#description').val() );
            // SE AGREGA AL JSON EL NOMBRE DEL PRODUCTO
            var postData = {
                "precio": 0,
                "unidades": 0,
                "modelo": "",
                "marca": "",
                "detalles": "",
                "imagen": ""
              };
            
            postData['nombre'] = $('#name').val();
            postData['precio'] = $('#precio').val();
            postData['unidades'] = $('#unidades').val();
            postData['modelo'] = $('#modelo').val();
            postData['marca'] = $('#marca').val();
            postData['detalles'] = $('#detalles').val();
            postData['imagen'] = $('#imagen').val();
            postData['id'] = $('#productId').val();
    
            /**
             * AQUÍ DEBES AGREGAR LAS VALIDACIONES DE LOS DATOS EN EL JSON
             * --> EN CASO DE NO HABER ERRORES, SE ENVIAR EL PRODUCTO A AGREGAR
             **/
    
            const url = edit === false ? './backend/product-add.php' : './backend/product-edit.php';
            
            $.post(url, postData, (response) => {
                //console.log(response);
                // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
                let respuesta = JSON.parse(response);
                // SE CREA UNA PLANTILLA PARA CREAR INFORMACIÓN DE LA BARRA DE ESTADO
                let template_bar = '';
                template_bar += `
                            <li style="list-style: none;">status: ${respuesta.status}</li>
                            <li style="list-style: none;">message: ${respuesta.message}</li>
                        `;
                // SE REINICIA EL FORMULARIO
                $('#name').val('');
                //$('#description').val(JsonString);
                // SE HACE VISIBLE LA BARRA DE ESTADO
                $('#product-result').show();
                // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
                $('#container').html(template_bar);
                // SE LISTAN TODOS LOS PRODUCTOS
                listarProductos();
                // SE REGRESA LA BANDERA DE EDICIÓN A false
                edit = false;
            });
        }
    });

    $(document).on('click', '.product-delete', (e) => {
        if(confirm('¿Realmente deseas eliminar el producto?')) {
            const element = $(this)[0].activeElement.parentElement.parentElement;
            const id = $(element).attr('productId');
            $.post('./backend/product-delete.php', {id}, (response) => {
                $('#product-result').hide();
                listarProductos();
            });
        }
    });

    $(document).on('click', '.product-item', (e) => {
        const element = $(this)[0].activeElement.parentElement.parentElement;
        const id = $(element).attr('productId');
        $.post('./backend/product-single.php', {id}, (response) => {
            // SE CONVIERTE A OBJETO EL JSON OBTENIDO
            let product = JSON.parse(response);
            // SE INSERTAN LOS DATOS ESPECIALES EN LOS CAMPOS CORRESPONDIENTES
            $('#name').val(product.nombre);
            // EL ID SE INSERTA EN UN CAMPO OCULTO PARA USARLO DESPUÉS PARA LA ACTUALIZACIÓN
            $('#productId').val(product.id);
            // SE ELIMINA nombre, eliminado E id PARA PODER MOSTRAR EL JSON EN EL <textarea>
            delete(product.nombre);
            delete(product.eliminado);
            delete(product.id);
            // SE CONVIERTE EL OBJETO JSON EN STRING
            let JsonString = JSON.stringify(product,null,2);
            // SE MUESTRA STRING EN EL <textarea>
            //$('#description').val(JsonString);
            
            // SE PONE LA BANDERA DE EDICIÓN EN true
            edit = true;
        });
        e.preventDefault();
    });    
});