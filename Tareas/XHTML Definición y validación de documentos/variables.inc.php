<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <?php
    echo ("<title>Practica 3: Manejo de variables con PHP</title>");
    ?>
</head>

<body>
    <?php
    #####################################
    $_myvar;
    #ESTA VARIABLE ES CORRECTA,
    #YA QUE INICIA CON UN UNDERSCORE
    $_7var;
    #ESTA VARIABLE ES CORRECTA,
    #INICIA CON UNDERSCORE
    #myvar;
    #NO SE PUEDE CONSIDERAR COMO VARIABLE, YA QUE NO TIENE EL SIGNO "$"
    # ANTES DEL NOMBRE DE LA VARIABLE POR LO TANTO, NO ESTA DECLARADA
    $var7;
    #LA VARIABLE ES VALIDA, INICIA CON EL SIGNO "$" Y CON LETRA
    $_elementl;
    #LA VARIABLE ES VALIDA, INICIA CON UNDERSCORE
    #$house*5;
    #LA VARIABLE NO ES VALIDA, ESTA INICIALIZADA BIEN, PERO
    #UNA VARIABLE NO PUEDE LLEVAR UN OPERADOR.
    #####################################
    echo ("
    <div>
        <p>\$_myvar: ESTA VARIABLE ES CORRECTA, YA QUE INICIA CON UN UNDERSCORE</p>
    </div>");

    echo ("
    <div>
        <p>\$_7var: ESTA VARIABLE ES CORRECTA, INICIA CON UN UNDERSCORE</p>
    </div>");

    echo ("
    <div>
        <p>myvar: NO SE PUEDE CONSIDERAR COMO VARIABLE, YA QUE NO TIENE EL SIGNO '$' ANTES DEL NOMBRE DE LA VARIABLE POR LO TANTO, NO ESTA DECLARADA</p>
    </div>");

    echo ("
    <div>
        <p>\$var7: LA VARIABLE ES VALIDA, INICIA CON EL SIGNO '$' Y CON LETRA</p>
    </div>");

    echo ("
    <div>
        <p>\$_elementl: LA VARIABLE ES VALIDA, INICIA CON UNDERSCORE</p>
    </div>");

    echo ("
    <div>
        <p>\$house*5: LA VARIABLE NO ES VALIDA, ESTA INICIALIZADA BIEN, PERO UNA VARIABLE NO PUEDE LLEVAR UN OPERADOR COMO PARTE DEL NOMBRE</p>
    </div>");
    ?>