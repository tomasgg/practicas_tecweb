    <?php
    unset($a, $b, $c);
    $a = "PHP5";
    echo ("
    <div>
        <p>Variable \$a: $a</p>
    </div>");

    $z[] = &$a;
    echo ("
    <div>
        <p>Arreglo \$z: </p><pre>");
    print_r($z);
    echo ("</pre>
    </div>");

    $b = "5a version de PHP";
    echo ("
    <div>
        <p>Variable \$b: $b</p>
    </div>");

    $c = $b * 10;
    echo ("
    <div>
        <p>Variable \$c: $c</p>
    </div>");

    $a .= $b;
    echo ("
    <div>
        <p>Variable \$a: $a</p>
    </div>");

    $b *= $c;
    echo ("
    <div>
        <p>Variable \$b: $b</p>
    </div>");

    $z[0] = "MySQL";
    echo ("
    <div>
        <p>Arreglo \$z: </p><pre>");
    print_r($z);
    echo ("</pre>
    </div>");
    ?>