    <?php
    unset($a, $b, $c);
    $a = "ManejadorSQL";
    $b = 'MySQL';
    $c = &$a;
    echo ("
    <div>
        <p>Variable \$a: $a </p>
        <p>Variable \$b: $b </p>
        <p>Variable \$c: $c </p>
    </div>");
    $a = "PHP server";
    $b = &$a;
    echo ("
    <div>
        <p>Variable \$a: $a </p>
        <p>Variable \$b: $b </p>
        <p>Variable \$c: $c </p>
    </div>");
    echo ("
    <div>
        <p>Como se observa en los valores de las variables, a y b se les da un valor, mientras que c se iguala referenciando a, significa que c tendra el mismo valor de a, en la segunda parte, se asigna un nuevo valor para a, y se iguala b referenciando a, al igual que c estos toman el valor que tiene a.</p>
    </div>");
    #######################################################################################
    #Como se observa en los valores de las variables, a y b se les da un valor,
    #mientras que c se iguala referenciando a, significa que c tendra el mismo valor de a,
    #en la segunda parte, se asigna un nuevo valor para a, y se iguala b referenciando a,
    #al igual que c estos toman el valor que tiene a.
    #######################################################################################
    ?>