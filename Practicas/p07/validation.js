function limitarName(text)
{
    if(text.value.length > 100)
    {
        text.value = text.value.substr(0, 100);
        // alert('El maximo de caracteres es 25');
    }
}

function comprobarSelect()
{
    var select = document.getElementById('marca');
    if (select.value == 0 || select.value == "")
    {
        alert("No se selecciono marca")
    }
}

function limitarMod(text)
{
    if(text.value.length > 25)
    {
        text.value = text.value.substr(0, 25);
        // alert('El maximo de caracteres es 25');
    }
}

function comprobarNum()
{
    var num = document.getElementById('precio');
    if (num.value < 99.99)
    {
    	num.style.borderColor  = "red";
        return false;
    }
    else
    {
        num.style.borderColor  = "green";
        return true;
    }
}

function limitarDet(text)
{
    if(text.value.length > 250)
    {
        text.value = text.value.substr(0, 250);
        // alert('El maximo de caracteres es 100');
    }   
}

function comprobarNum2()
{
    var num = document.getElementById('unidades');
    if (num.value < 0)
    {
    	num.style.borderColor  = "red";
        return false;
    }
    else
    {
        num.style.borderColor  = "green";
        return true;
    }
}

function comprobarFile(file)
{
    if (file.value == null)
    {
    	file.value = "https://pharmamex.com/images/default.png";
    }
}

function validData()
{
    if (comprobarSelect() == false || comprobarNum() == false || comprobarNum2() == false)
    {
        if (comprobarSelect() === false)
        {
            alert("Selecciona la marca. Default: Pendiente");
        }
        if (comprobarNum() === false)
        {
            alert("El precio debe ser mayor a $ 99.99. Default: $ 100.00");
        }
        if (comprobarNum2() === false)
        {
            alert("Las Unidades deben ser mayor o igual a 0. Default: 0")
        }
        return false;
    }
}