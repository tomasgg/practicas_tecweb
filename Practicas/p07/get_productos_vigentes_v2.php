<?php
$data = array();

$tope = 1;

if (!empty($tope)) {
    /** SE CREA EL OBJETO DE CONEXION */
    @$link = new mysqli('localhost', 'root', '12345678a', 'marketzone');
    /** NOTA: con @ se suprime el Warning para gestionar el error por medio de código */

    /** comprobar la conexión */
    if ($link->connect_errno) {
        die('Falló la conexión: ' . $link->connect_error . '<br/>');
        //exit();
    }

    /** Crear una tabla que no devuelve un conjunto de resultados */
    if ($result = $link->query("SELECT * FROM productos WHERE eliminados = 0")) {
        /** Se extraen las tuplas obtenidas de la consulta */
        $row = $result->fetch_all(MYSQLI_ASSOC);

        /** Se crea un arreglo con la estructura deseada */
        foreach ($row as $num => $registro) {            // Se recorren tuplas
            foreach ($registro as $key => $value) {      // Se recorren campos
                $data[$num][$key] = utf8_encode($value);
            }
        }

        /** útil para liberar memoria asociada a un resultado con demasiada información */
        $result->free();
    }

    $link->close();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<header>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <?php
    echo ('<title>Productos Vigentes</title>');
    ?>
    <script>
        function mod(){
            var rowId = event.target.parentNode.parentNode.id;

            var data = document.getElementById(rowId).querySelectorAll(".row-data");

            var id = data[0].innerHTML;
            var nombre = data[1].innerHTML;
            var marca = data[2].innerHTML;
            var modelo = data[3].innerHTML;
            var precio = data[4].innerHTML;
            var unidades = data[5].innerHTML;
            var detalles = data[6].innerHTML;
            var img = data[7].firstChild.getAttribute('src');

            take(id, nombre, marca, modelo, precio, unidades, detalles, img);
        }

        function take(id, nombre, marca, modelo, precio, unidades, detalles, img) {
            var urlForm = "http://localhost/TecWeb/Practicas/p07/formulario_productos_v2.php";
            var propId = "id=" + id;
            var propNombre = "nombre=" + nombre;
            var propMarca = "marca=" + marca;
            var propModelo = "modelo=" + modelo;
            var propPrecio = "precio=" + precio;
            var propUnidades = "unidades=" + unidades;
            var propDetalles = "detalles=" + detalles;
            var propImg = "img=" + img;

            window.open(urlForm + "?" + propId + "&" + propNombre + "&" + propMarca + "&" + propModelo + "&" + propPrecio + "&" + propUnidades + "&" + propDetalles + "&" + propImg);
        }
    </script>

</header>

<body>
    <h1>Productos Vigentes</h1>
    <br />

    <?php if (isset($row)) : ?>

        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Marca</th>
                    <th scope="col">Modelo</th>
                    <th scope="col">Precio</th>
                    <th scope="col">Unidades</th>
                    <th scope="col">Detalles</th>
                    <th scope="col">Eliminado</th>
                    <th scope="col">Imagen</th>
                    <th scope="col" class="text-center">Modificar</th>
                </tr>
            </thead>
            <?php
            echo ('
                    <tbody>');
            foreach ($data as $key => $value) {
                echo ('
                            <tr id=' . $data[$key]['id'] . '>
                                <td class="row-data">' . $data[$key]['id'] . '</td>
                                <td class="row-data">' . $data[$key]['nombre'] . '</td>
                                <td class="row-data">' . $data[$key]['marca'] . '</td>
                                <td class="row-data">' . $data[$key]['modelo'] . '</td>
                                <td class="row-data">' . $data[$key]['precio'] . '</td>
                                <td class="row-data">' . $data[$key]['unidades'] . '</td>
                                <td class="row-data">' . $data[$key]['detalles'] . '</td>
                                <td>' . $data[$key]['eliminados'] . '</td>
                                <td class="row-data" align="center"><img width="100px" height="auto" src=' . $data[$key]['imagen'] . '></td>
                                <td><input type="button" class="btn btn-outline-dark btn-sm" onclick="mod()" value="Modificar"></td>
                                </tr>
                            ');
            };
            echo ('</tbody>
                ');
            ?>
        </table>

    <?php elseif (!empty($id)) : ?>

        <script>
            alert('El ID del producto no existe');
        </script>

    <?php endif; ?>
</body>

</html>