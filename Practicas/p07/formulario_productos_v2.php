<?php
@$marca =  !empty($_POST['marca']) ? $_POST['marca'] : $_GET['marca'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">

    <title>Nuevo Producto</title>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col align-self-center text-center" style="padding-top: 1rem;">
                <h1>Datos del nuevo Producto</h1>
            </div>
        </div>
        <div class="conteiner" style="margin-top: 2rem;">

            <form action="set_producto_v2.php" method="post">
                <div class="input-group mb-3">
                    <span class="input-group-text" id="spanText" style="width: 20%;">ID</span>
                    <input type="number" class="form-control" name="id" value="<?= !empty($_POST['id']) ? $_POST['id'] : $_GET['id'] ?>" disabled>
                </div>

                <div class="input-group mb-3">
                    <span class="input-group-text" id="spanText" style="width: 20%;">Nombre del Producto</span>
                    <input type="text" class="form-control" placeholder="Nombre" name="nombre" value="<?= !empty($_POST['nombre']) ? $_POST['nombre'] : $_GET['nombre'] ?>">
                </div>

                <div class="input-group mb-3">
                    <?php
                    ob_start();
                    ?>
                    <label class="input-group-text" id="labelText" for="marca" style="width: 20%;">Marca del Producto</label>

                    <select class="form-select" name="marca" id="marca">
                        <option value="0" hidden> -- Selecciona -- </option>
                        <option value="CK">Calvin Klein</option>
                        <option value="AD">Adidas</option>
                        <option value="LV">Levi's</option>
                        <option value="LT">Lacoste</option>
                        <option value="AE">Aeropostale</option>
                        <option value="KM">King Monster</option>
                        <option value="ML">Mascara de Latex</option>
                        <option value="AG">¡ AY GÜEY !</option>
                    </select>
                    <?php
                    $select = ob_get_contents();
                    ob_end_clean();
                    str_replace('option value="' . $marca . '"', 'option value="' . $marca . '" selected', $select);
                    echo $select;
                    ?>
                </div>

                <div class="input-group mb-3">
                    <span class="input-group-text" id="spanText" style="width: 20%;">Modelo</span>
                    <input type="text" class="form-control" placeholder="Modelo" name="modelo" value="<?= !empty($_POST['modelo']) ? $_POST['modelo'] : $_GET['modelo'] ?>">
                </div>

                <div class="input-group mb-3">
                    <span class="input-group-text" id="spanText" style="width: 20%;">Precio &nbsp;$</span>
                    <input type="number" step="0.01" class="form-control" placeholder="0.00" name="precio" value="<?= !empty($_POST['precio']) ? $_POST['precio'] : $_GET['precio'] ?>">
                </div>

                <label for="" class="form-label">Detalles del Producto</label>
                <div class="input-group">
                    <span class="input-group-text" id="spanText" style="width: 20%;">Detalles</span>
                    <input type="text" class="form-control" placeholder="Detalles" name="detalles" value="<?= !empty($_POST['detalles']) ? $_POST['detalles'] : $_GET['detalles'] ?>">
                </div>

                <div class="input-group mb-3">
                    <input type="number" class="form-control" placeholder="Unidades" name="unidades" value="<?= !empty($_POST['unidades']) ? $_POST['unidades'] : $_GET['unidades'] ?>">
                    <span class="input-group-text">pz</span>
                    <input type="file" class="form-control" name="img" value="<?= !empty($_POST['img']) ? $_POST['img'] : $_GET['img'] ?>">
                </div>
                <div class="col-12">
                    <input class="btn btn-primary" type="submit" value="Actualizar Producto" />
                </div>
            </form>
        </div>
    </div>
</body>

</html>