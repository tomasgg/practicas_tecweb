<?php
$data = array();

if (isset($_GET['tope'])) {
    $tope = $_GET['tope'];
} else {
    $tope = 500;
    //die('Parámetro "tope" no detectado...');
}

if (!empty($tope)) {
    /** SE CREA EL OBJETO DE CONEXION */
    @$link = new mysqli('localhost', 'root', '12345678a', 'marketzone');
    /** NOTA: con @ se suprime el Warning para gestionar el error por medio de código */

    /** comprobar la conexión */
    if ($link->connect_errno) {
        die('Falló la conexión: ' . $link->connect_error . '<br/>');
        //exit();
    }

    /** Crear una tabla que no devuelve un conjunto de resultados */
    if ($result = $link->query("SELECT * FROM productos WHERE unidades <= $tope")) {
        /** Se extraen las tuplas obtenidas de la consulta */
        $row = $result->fetch_all(MYSQLI_ASSOC);

        /** Se crea un arreglo con la estructura deseada */
        foreach ($row as $num => $registro) {            // Se recorren tuplas
            foreach ($registro as $key => $value) {      // Se recorren campos
                $data[$num][$key] = utf8_encode($value);
            }
        }

        /** útil para liberar memoria asociada a un resultado con demasiada información */
        $result->free();
    }

    $link->close();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<header>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <script>
        function mod() {
            var rowId = event.target.parentNode.parentNode.id;

            var data = document.getElementById(rowId).querySelectorAll(".row-data");

            var id = data[0].innerHTML;
            var nombre = data[1].innerHTML;
            var marca = data[2].innerHTML;
            var modelo = data[3].innerHTML;
            var precio = data[4].innerHTML;
            var detalles = data[5].innerHTML;
            var unidades = data[6].innerHTML;
            var img = data[7].firstChild.getAttribute('src');

            take(id, nombre, marca, modelo, precio, detalles, unidades, img);
        }

        function take(id, nombre, marca, modelo, precio, detalles, unidades, img) {
                var form = document.createElement("form");

                var idIn = document.createElement("input");
                idIn.type = 'number';
                idIn.name = 'id';
                idIn.value = id;
                form.appendChild(idIn);

                var nombreIn = document.createElement("input");
                nombreIn.type = 'text';
                nombreIn.name = 'nombre';
                nombreIn.value = nombre;
                form.appendChild(nombreIn);
                
                var marcaIn = document.createElement("input");
                nombreIn.type = 'text';
                marcaIn.name = 'marca';
                marcaIn.value = marca;
                form.appendChild(marcaIn);

                var modeloIn = document.createElement("input");
                modeloIn.type = 'text';
                modeloIn.name = 'modelo';
                modeloIn.value = modelo;
                form.appendChild(modeloIn);

                var preIn = document.createElement("input");
                preIn.type = 'number';
                preIn.name = 'precio';
                preIn.value = precio;
                form.appendChild(preIn);

                var detallesIn = document.createElement("input");
                detallesIn.type = 'text';
                detallesIn.name = 'detalles';
                detallesIn.value = detalles;
                form.appendChild(detallesIn);

                var unidadesIn = document.createElement("input");
                unidadesIn.type = 'number';
                unidadesIn.name = 'unidades';
                unidadesIn.value = unidades;
                form.appendChild(unidadesIn);

                var imgIn = document.createElement("input");
                imgIn.type = 'text';
                imgIn.name = 'img';
                imgIn.value = img;
                form.appendChild(imgIn);

                console.log(form);

                form.method = 'POST';
                form.action = 'http://localhost/Tecweb/Practicas/p07/formulario_productos_v2.php?img=' + img;  

                document.body.appendChild(form);
                form.submit();
            }
    </script>
    <?php
    echo ('<title>Producto</title>');
    ?>
</header>

<body>
    <h1>Productos</h1>
    <br />

    <?php if (isset($row)) : ?>

        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Marca</th>
                    <th scope="col">Modelo</th>
                    <th scope="col">Precio</th>
                    <th scope="col">Unidades</th>
                    <th scope="col">Detalles</th>
                    <th scope="col">Imagen</th>
                    <th scope="col" class="text-center">Modificar</th>
                </tr>
            </thead>
            <?php
            echo ('
                    <tbody>');
            foreach ($data as $key => $value) {
                echo ('
                            <tr id=' . $data[$key]['id'] . '>
                                <td class="row-data" id="id">' . $data[$key]['id'] . '</td>
                                <td class="row-data" id="nombre">' . $data[$key]['nombre'] . '</td>
                                <td class="row-data" id="marca">' . $data[$key]['marca'] . '</td>
                                <td class="row-data" id="modelo">' . $data[$key]['modelo'] . '</td>
                                <td class="row-data" id="precio">' . $data[$key]['precio'] . '</td>
                                <td class="row-data" id="detalles">' . $data[$key]['detalles'] . '</td>
                                <td class="row-data" id="unidades">' . $data[$key]['unidades'] . '</td>
                                <td class="row-data" id="img" align="center"><img width="100px" height="auto" src=' . $data[$key]['imagen'] . '></td>
                                <td><input type="button" class="btn btn-outline-dark btn-sm" onclick="mod()" value="Modificar"></td>
                            </tr>
                            ');
            };
            echo ('</tbody>
                ');
            ?>
        </table>

    <?php elseif (!empty($id)) : ?>

        <script>
            alert('El ID del producto no existe');
        </script>
    <?php endif; ?>
</body>

</html>