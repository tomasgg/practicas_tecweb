<?php
@$id = !empty($_POST['id']) ? $_POST['id'] : $_GET['id'];
@$link = new mysqli('localhost', 'root', '12345678a', 'marketzone');

if ($link->connect_errno) 
{
    die('Falló la conexión: '.$link->connect_error.'<br/>');
}

if ( $result = $link->query("SELECT * FROM productos WHERE id = $id") ) 
{
    $row = $result->fetch_all(MYSQLI_ASSOC);

    foreach($row as $num => $registro) {
        foreach($registro as $key => $value) {
            $data[$num][$key] = utf8_encode($value);
        }
    }

    $result->free();
}

$link->close();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    
    <title>Modificar Producto</title>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col align-self-center text-center" style="padding-top: 1rem;">
                <h1>Actualizacion de Datos</h1>
            </div>
        </div>
        <div class="conteiner" style="margin-top: 2rem;">

            <form action="set_producto_v3.php" method="post">
                <div class="input-group mb-3">
                    <span class="input-group-text" id="spanText" style="width: 20%;">ID</span>
                    <input type="number" class="form-control" name="id" 
                    <?php foreach ($data as $key => $value)
                    {
                        echo("value= '" . $data[$key]['id'] . "'");
                    }?> readonly="readonly">
                </div>
                
                <div class="input-group mb-3">
                    <span class="input-group-text" id="spanText" style="width: 20%;">Nombre del Producto</span>
                    <input type="text" class="form-control" placeholder="Nombre" name="nombre"
                    <?php foreach ($data as $key => $value)
                    {
                        echo("value= '" . $data[$key]['nombre'] . "'");
                    }?>>
                </div>

                <div class="input-group mb-3">
                    <span class="input-group-text" id="spanText" style="width: 20%;">Marca del Producto</span>
                    <?php
                        ob_start();
                    ?>
                    <select class="form-select" name="marca" id="marca">
                        <option value="0" hidden> -- Selecciona -- </option>
                        <option value="CK">Calvin Klein</option>
                        <option value="AD">Adidas</option>
                        <option value="LV">Levi's</option>
                        <option value="LT">Lacoste</option>
                        <option value="AE">Aeropostale</option>
                        <option value="KM">King Monster</option>
                        <option value="ML">Mascara de Latex</option>
                        <option value="AG">¡ AY GÜEY !</option>
                    </select>
                    <?php
                        $select = ob_get_contents();
                        ob_end_clean();
                        str_replace('value="' . $data[$key]['marca'] . '"', 'value="' . $data[$key]['marca'] . '" selected', $select);
                        echo $select;
                    ?>
                </div>

                <div class="input-group mb-3">
                    <span class="input-group-text" id="spanText" style="width: 20%;">Modelo</span>
                    <input type="text" class="form-control" placeholder="Modelo" name="modelo"
                    <?php foreach ($data as $key => $value)
                    {
                        echo("value= '" . $data[$key]['modelo'] . "'");
                    }?>>
                </div>

                <div class="input-group mb-3">
                    <span class="input-group-text" id="spanText" style="width: 20%;">Precio &nbsp;$</span>
                    <input type="number" step="0.01" class="form-control" placeholder="0.00" name="precio"
                    <?php foreach ($data as $key => $value)
                    {
                        echo("value= '" . $data[$key]['precio'] . "'");
                    }?>>
                </div>

                <label for="" class="form-label">Detalles del Producto</label>
                <div class="input-group">
                    <span class="input-group-text" id="spanText" style="width: 20%;">Detalles</span>
                    <input type="text" class="form-control" placeholder="Detalles" name="detalles"
                    <?php foreach ($data as $key => $value)
                    {
                        echo("value= '" . $data[$key]['detalles'] . "'");
                    }?>>
                </div>

                <div class="input-group mb-3">
                    <input type="number" class="form-control" placeholder="Unidades" name="unidades"
                    <?php foreach ($data as $key => $value)
                    {
                        echo("value= '" . $data[$key]['unidades'] . "'");
                    }?>>
                    <span class="input-group-text">pz</span>
                    <input type="file" class="form-control" name="img"
                    <?php foreach ($data as $key => $value)
                    {
                        echo("value= '" . $data[$key]['id'] . "'");
                    }?>>
                </div>
                <div class="col-12">
                    <input class="btn btn-primary" type="submit" value="Actualizar Producto" />
                </div>
            </form>
        </div>
    </div>
</body>

</html>