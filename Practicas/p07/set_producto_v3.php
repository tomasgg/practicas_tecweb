<?php
@$id = $_POST["id"];
@$nombre = $_POST["nombre"];
@$marca = $_POST["marca"];
@$modelo = $_POST["modelo"];
@$precio = floatval($_POST["precio"]);
@$detalles = $_POST["detalles"];
@$unidades = intval($_POST["unidades"]);
@$img = 'img/' . $_POST["img"];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>Estado de Subida</title>
</head>

<body>
    <div class="container">
        <?php
        if ($nombre != null && $marca != null && $modelo != null && $precio != null && $unidades != null) {
            echo ('
                <div class="col align-self-center text-center" style="padding-top: 1rem;">
                <div class="container">
                <div class="col align-self-center text-center" style="padding-top: 1rem;">
                <h2>Datos del nuevo Producto</h2>
                </div>
                </div>
                <h3>Los datos estan correctos :D</h3>
                ');
            echo ('
                <table class="table table-dark table-hover">
                <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Marca</th>
                <th scope="col">Modelo</th>
                <th scope="col">Precio</th>
                <th scope="col">Detalles</th>
                <th scope="col">Unidades</th>
                <th scope="col">Imagen</th>
                </tr>
                </thead>
                ');
            echo ('
                <tbody>
                <tr>
                <th scope="row">'. $_POST['id'] .'</th>
                <td>
                ');
            echo ($nombre);
            echo ('
                </td>
                <td>
                ');
            if ($marca == 0) {
                $marca = "Pendiente";
                echo ($marca);
            } else {
                echo ($marca);
            }
            echo ('
                </td>
                <td>
                ');
            echo ($modelo);
            echo ('
                </td>
                <td>
                ');
            if ($precio < 100) {
                $precio = 100.00;
                echo ($precio);
            } else {
                echo ($precio);
            }
            echo ('
                </td>
                <td>
                ');
            echo ($detalles);
            echo ('
                </td>
                <td>
                ');
            if ($unidades < 0) {
                $unidades = 0;
                echo ($unidades);
            } else {
                echo ($unidades);
            }
            echo ('
                </td>
                <td>
                ');
            if ($img == "img/") {
                $img = "https://pharmamex.com/images/default.png";
                echo ("DEFAULT");
            } else {
                echo ($img);
            }
            echo ('
                </td>
                ');
            echo ('
                </tr>
            <tr>
                <th scope="row">Tipo</th>
                <td>
                ');
            echo (gettype($nombre));
            echo ('
                </td>
                <td>
                ');
            echo (gettype($marca));
            echo ('
                </td>
                <td>
                ');
            echo (gettype($modelo));
            echo ('
                </td>
                <td>
                ');
            echo (gettype($precio));
            echo ('
                </td>
                <td>
                ');
            echo (gettype($detalles));
            echo ('
                </td>
                <td>
                ');
            echo (gettype($unidades));
            echo ('
                </td>
                <td>
                ');
            echo (gettype($img));
            echo ('
                </td>
                ');
            echo ('
                </tr>
                </tbody>
                </table>
                ');

            @$link = new mysqli('localhost', 'root', '12345678a', 'marketzone');

            if ($link === false) {
                die('Falló la conexión: ' . mysqli_connect_error());
            }

            $sql = "UPDATE productos  SET nombre='$nombre', marca='$marca', modelo='$modelo', precio='$precio', detalles='$detalles', unidades='$unidades', imagen='$img', eliminados=0 WHERE id='$id'";
            if (mysqli_query($link, $sql)) {
                echo ('
                <p>
                Producto actualizado con ID: '  . $id .
                    '</p>');
            }
            else
            {
                echo ('
                <p>
                ERROR en la actualizacion ID: ' . $id . '
                    </p>');
            }
            mysqli_close($link);
        } ?>
        <a href="http://localhost/phpmyadmin/sql.php?db=marketzone&table=productos&pos=0"><input class="btn btn-secondary" type="button" value="Ver base de datos"></button></a>
</body>

</html>