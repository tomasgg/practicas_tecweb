<?php
    use API\Read\Read as Productos;
    require_once __DIR__.'/API/Read/Read.php';

    $productos = new Productos('marketzone');
    $productos->list();
    echo $productos->getResponse();
?>