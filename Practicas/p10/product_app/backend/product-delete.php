<?php
    use API\Delete\Delete as Productos;
    require_once __DIR__.'/API/Delete\Delete.php';

    $productos = new Productos('marketzone');
    $productos->delete( $_POST['id'] );
    echo $productos->getResponse();
?>