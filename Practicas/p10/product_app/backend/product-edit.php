<?php
    use API\Update\Update as Productos;
    require_once __DIR__.'/API/Update/Update.php';

    $productos = new Productos('marketzone');
    $productos->edit( json_decode( json_encode($_POST) ) );
    echo $productos->getResponse();
?>