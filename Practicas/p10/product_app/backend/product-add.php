<?php
    use API\Create\Create as Productos;
    require_once __DIR__.'/API/Create/Create.php';

    $productos = new Productos('marketzone');
    $productos->add( json_decode( json_encode($_POST) ) );
    echo $productos->getResponse();
?>