<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>Nuevo Producto</title>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col align-self-center text-center" style="padding-top: 1rem;">
                <h1>Datos del nuevo Producto</h1>
            </div>
        </div>
        <div class="conteiner" style="margin-top: 2rem;">

            <form action="http://localhost/Tecweb/Practicas/p06/set_producto_v2.php" method="post">
                <div class="input-group mb-3">
                    <span class="input-group-text" id="spanText" style="width: 20%;" >Nombre del Producto</span>
                    <input type="text" class="form-control" placeholder="Nombre" name="nombre">
                </div>

                <div class="input-group mb-3">
                    <span class="input-group-text" id="spanText" style="width: 20%;" >Marca del Producto</span>
                    <input type="text" class="form-control" placeholder="Marca" name="marca">
                </div>

                <div class="input-group mb-3">
                    <span class="input-group-text" id="spanText" style="width: 20%;" >Modelo</span>
                    <input type="text" class="form-control" placeholder="Modelo" name="modelo">
                </div>

                <div class="input-group mb-3">
                    <span class="input-group-text" id="spanText" style="width: 20%;" >Precio &nbsp;$</span>
                    <input type="number" step="0.01" class="form-control" placeholder="0.00" name="precio">
                </div>

                <label for="" class="form-label">Detalles del Producto</label>
                <div class="input-group">
                    <span class="input-group-text" id="spanText" style="width: 20%;" >Detalles</span>
                    <input type="text" class="form-control" placeholder="Detalles" name="detalles"></textarea>
                </div>

                <div class="input-group mb-3">
                    <input type="number" class="form-control" placeholder="Unidades" name="unidades">
                    <span class="input-group-text">pz</span>
                    <input type="file" class="form-control" name="img">
                </div>
                <div class="col-12">
                    <input class="btn btn-primary" type="submit" value="Crear nuevo Producto"/>
                </div>
            </form>
        </div>
    </div>
</body>

</html>