<?php
@$name = $_POST["nombre"];
@$marca = $_POST["marca"];
@$modelo = $_POST["modelo"];
@$precio = floatval($_POST["precio"]);
@$detalles = $_POST["detalles"];
@$unidades = intval($_POST["unidades"]);
@$img = 'img/' . $_POST["img"];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>Estado de Subida</title>
</head>

<body>
    <div class="container">
        <?php
        if ($name != null && $marca != null && $modelo != null && $precio != null && $detalles != null && $unidades != null && $img != null && $img != "img/") {
            echo ('
                <div class="col align-self-center text-center" style="padding-top: 1rem;">
                <div class="container">
                <div class="col align-self-center text-center" style="padding-top: 1rem;">
                <h2>Datos del nuevo Producto</h2>
                </div>
                </div>
                <h3>Los datos estan correctos :D</h3>
                ');
            echo ('
                <table class="table table-dark table-hover">
                <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Marca</th>
                <th scope="col">Modelo</th>
                <th scope="col">Precio</th>
                <th scope="col">Detalles</th>
                <th scope="col">Unidades</th>
                <th scope="col">Imagen</th>
                </tr>
                </thead>
                ');
            echo ('
                <tbody>
                <tr>
                <th scope="row">1</th>
                <td>
                ');
            if ($name != null) {
                echo ($name);
            } else {
                echo ('V A C I O');
            }
            echo ('
                </td>
                <td>
                ');
            if ($marca != null) {
                echo ($marca);
            } else {
                echo ('V A C I O');
            }
            echo ('
                </td>
                <td>
                ');
            if ($modelo != null) {
                echo ($modelo);
            } else {
                echo ('V A C I O');
            }
            echo ('
                </td>
                <td>
                ');
            if ($precio != null) {
                echo ($precio);
            } else {
                echo ('V A C I O');
            }
            echo ('
                </td>
                <td>
                ');
            if ($detalles != null) {
                echo ($detalles);
            } else {
                echo ('V A C I O');
            }
            echo ('
                </td>
                <td>
                ');
            if ($unidades != null) {
                echo ($unidades);
            } else {
                echo ('V A C I O');
            }
            echo ('
                </td>
                <td>
                ');
            if ($img != null) {
                echo ($img);
            } else {
                echo ('V A C I O');
            }
            echo ('
                </td>
                ');
            echo ('
                </tr>
            <tr>
                <th scope="row">Tipo</th>
                <td>
                ');
            echo (gettype($name));
            echo ('
                </td>
                <td>
                ');
            echo (gettype($marca));
            echo ('
                </td>
                <td>
                ');
            echo (gettype($modelo));
            echo ('
                </td>
                <td>
                ');
            echo (gettype($precio));
            echo ('
                </td>
                <td>
                ');
            echo (gettype($detalles));
            echo ('
                </td>
                <td>
                ');
            echo (gettype($unidades));
            echo ('
                </td>
                <td>
                ');
            echo (gettype($img));
            echo ('
                </td>
                ');
            echo ('
                </tr>
                </tbody>
                </table>
                ');

            @$link = new mysqli('localhost', 'root', '12345678a', 'marketzone');

            if ($link->connect_errno) {
                die('Falló la conexión: ' . $link->connect_error . '<br/>');
            }

            $sql = "INSERT INTO productos VALUES (null, '{$name}', '{$marca}', '{$modelo}', {$precio}, '{$detalles}', {$unidades}, '{$img}', 0)";
            if ($link->query($sql)) {
                echo ('
                <p>
                Producto insertado con ID: '  . $link->insert_id .  
                '</p>');
            } else {
                echo ('
                <p>
                El Producto no pudo ser insertado :(
                </p>');
            }

            $link->close();
        } else {
            echo ('
            <div class="conteiner" style="padding-top: 2rem;">
            ');
            if ($name == null) {
                echo ('
                    <h3>Alguno de los datos no es valido, NO se ingresara a la base de datos. :c (Nombre: ' . $name . ')</h3>
                    ');
            }
            if ($marca == null) {
                echo ('
                    <h3>Alguno de los datos no es valido, NO se ingresara a la base de datos. :c (Marca: ' . $marca . ')</h3>
                    ');
            }
            if ($modelo == null) {
                echo ('
                    <h3>Alguno de los datos no es valido, NO se ingresara a la base de datos. :c (Modelo: ' . $modelo . ')</h3>
                    ');
            }
            if ($precio == null) {
                echo ('
                    <h3>Alguno de los datos no es valido, NO se ingresara a la base de datos. :c (Precio: ' . $precio . ')</h3>
                    ');
            }
            if ($detalles == null) {
                echo ('
                    <h3>Alguno de los datos no es valido, NO se ingresara a la base de datos. :c (Detalles: ' . $detalles . ')</h3>
                    ');
            }
            if ($unidades == null) {
                echo ('
                    <h3>Alguno de los datos no es valido, NO se ingresara a la base de datos. :c (Unidades: ' . $unidades . ')</h3>
                    ');
            }
            if ($img == null || $img == "img/") {
                echo ('
                    <h3>Alguno de los datos no es valido, NO se ingresara a la base de datos. :c (Imagen: ' . $img . ')</h3>
                    ');
            }
            echo ('
            </div>
            ');
        }

        ?>
</body>

</html>