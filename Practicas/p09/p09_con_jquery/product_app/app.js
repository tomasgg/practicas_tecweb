// JSON BASE A MOSTRAR EN FORMULARIO
var baseJSON = {
    "precio": 0.0,
    "unidades": 1,
    "modelo": "XX-000",
    "marca": "NA",
    "detalles": "NA",
    "imagen": "img/default.png"
};

function init() {
    /**
     * Convierte el JSON a string para poder mostrarlo
     * ver: https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/JSON
     */
    var JsonString = JSON.stringify(baseJSON, null, 2);
    document.getElementById("description").value = JsonString;

    // SE LISTAN TODOS LOS PRODUCTOS
    //listarProductos();
};

$(document).ready(function () {

    console.log('JQuery is working');
    lista();
    $('#product-result').hide();
    let edit = false;

    $('#search').keyup(function (e) {
        if ($('#search').val()) {
            let search = $('#search').val();
            //console.log(search);
            $.ajax({
                url: './backend/product-search.php?search=' + search,
                type: 'GET',
                success: function (response) {
                    let productos = JSON.parse(response);
                    //console.log(productos);
                    let template = '';

                    productos.forEach(producto => {
                        let descripcion = '';
                        descripcion += '<li>precio: ' + producto.precio + '</li>';
                        descripcion += '<li>unidades: ' + producto.unidades + '</li>';
                        descripcion += '<li>modelo: ' + producto.modelo + '</li>';
                        descripcion += '<li>marca: ' + producto.marca + '</li>';
                        descripcion += '<li>detalles: ' + producto.detalles + '</li>';

                        template += `
                            <tr productId="${producto.id}">
                                <td>${producto.id}</td>
                                <td><a href="#" class="product-item">${producto.nombre}</a></td>
                                <td><ul>${descripcion}</ul></td>
                                <td>
                                    <button class="product-delete btn btn-danger">
                                        Eliminar
                                    </button>
                                </td>
                            </tr>
                        `;
                    })

                    $('#products').html(template);
                    
                }
            })
        }
    })

    $('#product-form').submit(function (e) {
        e.preventDefault();
        var producto = JSON.parse($('#description').val());
        producto['nombre'] = $('#name').val()

        //console.log(producto);
        let url = edit === false ? "./backend/product-add.php" : "./backend/product-edit.php";
        if (url == "./backend/product-edit.php") {
            producto['id'] = $('#productId').val()
            console.log(producto);
        }

        var productString = JSON.stringify(producto, null, 2);
        
        $.post(url, productString, function (response) {
            lista();
            $('#product-form').trigger('reset');

            init();
            console.log(response);
        })
    })

    function lista(){
        $.ajax({
            url: './backend/product-list.php',
            type: 'GET',
            success: function (response) {
                let productos = JSON.parse(response);
                //console.log(productos);
                let template = '';
    
                productos.forEach(producto => {
                    let descripcion = '';
                    descripcion += '<li>precio: ' + producto.precio + '</li>';
                    descripcion += '<li>unidades: ' + producto.unidades + '</li>';
                    descripcion += '<li>modelo: ' + producto.modelo + '</li>';
                    descripcion += '<li>marca: ' + producto.marca + '</li>';
                    descripcion += '<li>detalles: ' + producto.detalles + '</li>';
    
                    template += `
                                <tr productId="${producto.id}">
                                    <td>${producto.id}</td>
                                    <td nombre="${producto.nombre}"><a href="#" class="product-item" >${producto.nombre}</a></td>
                                    <td><ul>${descripcion}</ul></td>
                                    <td>
                                        <button class="product-delete btn btn-danger">
                                            Eliminar
                                        </button>
                                    </td>
                                </tr>
                            `;
                })
    
                $('#products').html(template);
            }
        })
    }

    $(document).on('click', '.product-delete', function () {
        if (confirm('¿Estas seguro de eliminar el producto?')) {
            let element = $(this)[0].parentElement.parentElement;
            let id = $(element).attr('productID');
            //console.log(id);
            $.ajax({
                url: './backend/product-delete.php?id=' + id,
                type: 'GET',
                success: function (response) {
                    lista();
                    console.log(response);
                }
            })
        }
    })

    $(document).on('click', '.product-item', function () {
        let element = $(this)[0].parentElement.parentElement;
        let id = $(element).attr('productID');
        let element2 = $(this)[0].parentElement;
        let name = $(element2).attr('nombre');
        $.post('./backend/product-single.php', {id}, function (response){
            const product = JSON.parse(response);
            $('#name').val(name);
            var productString = JSON.stringify(product, null, 2);
            $('#description').val(productString);
            $('#productId').val(id);
            console.log($('#productId').val());
            edit = true;
        })
    })

});
