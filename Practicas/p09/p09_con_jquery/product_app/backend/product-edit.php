<?php
    include_once __DIR__.'/database.php';

    // SE OBTIENE LA INFORMACIÓN DEL PRODUCTO ENVIADA POR EL CLIENTE
    $producto = file_get_contents('php://input');
    $data = array(
        'status'  => 'error',
        'message' => 'No se encontro el producto'
    );

    $jsonOBJ = json_decode($producto);
    $sql = "UPDATE productos  SET nombre='$jsonOBJ->nombre', marca='$jsonOBJ->marca', modelo='$jsonOBJ->modelo', precio='$jsonOBJ->precio', detalles='$jsonOBJ->detalles', unidades='$jsonOBJ->unidades', imagen='$jsonOBJ->imagen', eliminado=0 WHERE id ='$jsonOBJ->id'";
    $result = mysqli_query($conexion, $sql);

    if (!$result) {
        die('Query Error: '.mysqli_error($conexion));
    }
    else
    {
        $data['status'] =  "success";
        $data['message'] =  "Producto actualizado";
    }

    // SE HACE LA CONVERSIÓN DE ARRAY A JSON
    echo json_encode($data, JSON_PRETTY_PRINT);
?>