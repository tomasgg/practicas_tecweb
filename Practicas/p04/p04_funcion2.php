<?php
    unset($n_ran);
    $arreglo = array(array());
    $fil = 4;
    $col = 3;

    #Creacion de Matriz
    for($j = 0; $j< $fil; $j++) 
    {
        for($i = 0; $i< $col; $i++)
        {
            $arreglo[$j][$i] = 0;
        }
    }

    #Validacion y insercion de numeros random intercalados par / impar
    foreach ($arreglo as $key => $value) 
    {
        foreach ($arreglo[$key] as $i => $value2) 
        {
            $n_ran = random_int(1,1000);
            if ($i == 0 and $key == 0)
            {
                $arreglo[$key][$i] = $n_ran;
            }
            elseif ($key >= 0)
            {
                if ($i == 0 and $key > 0)
                {
                    if ($arreglo[$key-1][2]%2 == 0)
                    {
                        while($n_ran%2 == 0)
                        {
                            $n_ran = random_int(1,1000);
                        }
                        $arreglo[$key][$i] = $n_ran;
                    }
                    else if ($arreglo[$key-1][2]%2 != 0)
                    {
                        while($n_ran%2 != 0)
                        {
                            $n_ran = random_int(1,1000);
                        }
                        $arreglo[$key][$i] = $n_ran;
                    }
                }
                else
                {
                    if ($arreglo[$key][$i-1]%2 == 0)
                    {
                        if ($n_ran%2 != 0)
                        {
                            $arreglo[$key][$i] = $n_ran;
                        }
                        else
                        {
                            while($n_ran%2 == 0)
                            {
                                $n_ran = random_int(1,1000);
                            }
                            $arreglo[$key][$i] = $n_ran;
                        }
                    }
                    else
                    {
                        if ($n_ran%2 == 0)
                        {
                            $arreglo[$key][$i] = $n_ran;
                        }
                        else
                        {
                            while($n_ran%2 != 0)
                            {
                                $n_ran = random_int(1,1000);
                            }
                            $arreglo[$key][$i] = $n_ran;
                        }
                    }
                }
            }
        }
    };

    function printarray($arreglo)
    {
        echo ('           <pre>');
        foreach ($arreglo as $key => $value) 
        {
        foreach ($arreglo[$key] as $key2 => $value) 
        {
            echo(' [' . $arreglo[$key][$key2] . '] ' );
        }
        echo("
                <br>");
        }
        echo ('</pre>');
    };

    echo ('
        <div>
            <p>Matriz:<br>
    ');
    printarray($arreglo);
    echo ('
            </p>
        </div>
    ');
?>