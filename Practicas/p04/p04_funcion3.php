<?php
    unset($nran, $num);
    $num = $_GET['num'];
    $nran = rand();

    #While
    while ($nran%1 == 0 and $nran%$num != 0)
    {
        $nran = rand();
    };

    echo ('
        <div>
            <p>Numero: ' . $num . ' es multiplo de</p>
            <P>Numero random con While: ' . $nran . '</p>
        </div>
    ');

    #Alternativa DoWhile
    do
    {
        $nran = rand();
    } while ($nran%1 == 0 and $nran%$num != 0);

    echo ('
        <div>
            <p>Numero: ' . $num . ' es multiplo de</p>
            <P>Numero random con DoWhile: ' . $nran . '</p>
        </div>
    ');
?>