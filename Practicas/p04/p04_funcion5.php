<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php
    echo ("
        <title>Funcion 5: Metodo Post</title>
        <h2>Muestreo de datos ingresados en el Forms</h2>
    ");
    ?>
</head>
<body>
<?php
    $sexo = $_POST['sexo'];
    $edad = $_POST['edad'];

    echo('
    <div>
        <p>Sexo: ' . $sexo . '</p>
        <p>Edad: ' . $edad . ' </p>
    </div>
    ');
?>
</body>
</html>