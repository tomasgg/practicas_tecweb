<?php
    include_once __DIR__.'/database.php';

    // SE OBTIENE LA INFORMACIÓN DEL PRODUCTO ENVIADA POR EL CLIENTE
    $producto = file_get_contents('php://input');
    if(!empty($producto)) {
        // SE TRANSFORMA EL STRING DEL JASON A OBJETO
        $jsonOBJ = json_decode($producto, true);
        /**
         * SUSTITUYE LA SIGUIENTE LÍNEA POR EL CÓDIGO QUE REALICE
         * LA INSERCIÓN A LA BASE DE DATOS. COMO RESPUESTA REGRESA
         * UN MENSAJE DE ÉXITO O DE ERROR, SEGÚN SEA EL CASO.
         */
        @$link = new mysqli('localhost', 'root', '12345678a', 'marketzone');

        if ($link->connect_errno) {
            echo ("Error en la Conexion a la BD ");
            die('Falló la conexión: ' . $link->connect_error . '<br/>');
        }

        $sql = "INSERT INTO productos VALUES (null, '{$jsonOBJ['nombre']}', '{$jsonOBJ['marca']}', '{$jsonOBJ['modelo']}', {$jsonOBJ['precio']}, '{$jsonOBJ['detalles']}', {$jsonOBJ['unidades']}, '{$jsonOBJ['imagen']}', 0)";
        if ($link->query($sql)) { 
            echo ('Producto insertado con ID: ' . $link->insert_id . ' ');
        }
        $link->close();

        echo '[SERVIDOR] Nombre: '.$jsonOBJ['nombre'];
    }
?>